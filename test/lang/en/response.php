<?php
return [
    'success' => [
        'created'=>'Created successfully',
        'updated'=>'Updated successfully',
        'deleted'=>'Deleted successfully',
        'success'=>'Successfully',
    ],
    'error'=> [
        'error'=>'Error',
        'not_found'=>'Not found',
        'bad_request'=> 'Bad request',
        'unauthorized'=>'This action is unauthorized'
    ],
];
