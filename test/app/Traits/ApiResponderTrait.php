<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponderTrait
{
    public function responseSuccess($data = [], $message = 'success', $statusCode = Response::HTTP_OK): JsonResponse
    {
        $message = trans('response.success.' . $message);

        $info = [
            'message' => $message,
            'data' => $data,
        ];
        return response()->json($info, $statusCode);
    }

    public function responseError($message = null, $errors = 'error', $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        $message = trans('response.error.' . $message);
        if (!is_array($errors)) {
            $errors = [$errors];
        }
        return response()->json([
            'message' => $message,
            'errors' => $errors
        ], $statusCode);
    }


    public function responseUnauthorized($errors = null): JsonResponse
    {
        return $this->responseError('unauthorized', $errors, Response::HTTP_UNAUTHORIZED);
    }

    public function responseBadRequest($errors = null): JsonResponse
    {
        return $this->responseError('bad_request', $errors, Response::HTTP_BAD_REQUEST);
    }

    public function responseNotFound($errors = null): JsonResponse
    {
        return $this->responseError('not_found', $errors, Response::HTTP_NOT_FOUND);
    }

    public function responseUpdated($data = null): JsonResponse
    {
        return $this->responseSuccess($data, 'updated');
    }

    public function responseCreated($data = null): JsonResponse
    {
        return $this->responseSuccess($data, 'created', Response::HTTP_CREATED);
    }

    public function responseDeleted($data = null): JsonResponse
    {
        return $this->responseSuccess($data, 'deleted');
    }

    public function responseNoContent(): JsonResponse
    {
        return $this->responseSuccess(statusCode: Response::HTTP_NO_CONTENT);
    }


}
