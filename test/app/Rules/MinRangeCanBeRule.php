<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class MinRangeCanBeRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $results = request('results');
        $questionsCount = count(request('questions'));
        $minPointOfAnswer = min(request('point_of_answers'));
        $minMarkCanBe = $questionsCount * $minPointOfAnswer;
        if ($minMarkCanBe < min($results)['min_mark'] || $minMarkCanBe > min($results)['max_mark'])
            $fail('Min range is not valid');
    }
}
