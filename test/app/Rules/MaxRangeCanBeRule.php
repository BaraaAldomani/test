<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use function PHPUnit\Framework\isEmpty;

class MaxRangeCanBeRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $results = request('results');
        $questionsCount = count(request('questions'));
        $maxPointOfAnswer = max(request('point_of_answers'));
        $maxMarkCanBe = $questionsCount * $maxPointOfAnswer;
        if ($maxMarkCanBe < max($results)['min_mark'] || $maxMarkCanBe > max($results)['max_mark'])
            $fail('Max range is not valid');
    }
}
