<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class OverlapRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        foreach ($value as $result) {
            if (!isset($result['min_mark'],$result['max_mark'])){
                $fail('There is field with null value');
                return;
            }
        }
        $minMarks = array_column($value, 'min_mark');
        array_multisort($minMarks, $value);

        foreach ($value as $key => $result) {
            if ($key === count($value) - 1) return;
            if ($value[$key + 1]['min_mark'] < $result['max_mark']) {
                $fail('There is overlap');
            }
        }
    }
}
