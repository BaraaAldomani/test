<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Spatie\LaravelData\Data;

class TestData extends Data
{
    public ?int $id = null;

    public function __construct(
        public ?string $name,
        public ?string $description,
        public ?string $type,
        public ?int    $number_of_answers,
        public ?array  $point_of_answers)
    {
    }

    public static function fromRequest(FormRequest $request)
    {
        $data = $request->validated();
        return new self(
            name: $data['name'],
            description: $data['description'],
            type: $data['type'],
            number_of_answers: $data['number_of_answers'],
            point_of_answers: $data['point_of_answers'],
        );
    }

    public function pointsToJson()
    {
        $pointJson = [];
        foreach ($this->point_of_answers as $key => $point) {
            $pointJson[$key] = $point;
        }
        return $pointJson;
    }


}
