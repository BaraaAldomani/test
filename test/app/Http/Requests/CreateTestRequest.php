<?php

namespace App\Http\Requests;

use App\Enums\TestTypes;
use App\Rules\MaxRangeCanBeRule;
use App\Rules\MinRangeCanBeRule;
use App\Rules\MisRangeRule;
use App\Rules\OverlapRule;
use App\Rules\ResultsRule;
use App\Rules\ValidMaxMarkRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'max:125', 'unique:tests,name'],
            'description' => ['required'],
            'type' => ['required', new Enum(TestTypes::class)],
            'number_of_answers' => ['required', 'numeric',
                Rule::when(request('type') === TestTypes::MCQ->value, ['min:2', 'max:5']),
                Rule::when(request('type') === TestTypes::TRUE_FALSE->value, ['in:2']),
            ],
            'point_of_answers' => ['required', 'array', 'size:' . request('number_of_answers')],
            'point_of_answers.*' => ['numeric'],
            'questions' => ['required'],
            'questions.*.question_text' => [
                'required',
            ],
            'questions.*.answers' => ['required', 'array', 'size:' . request('number_of_answers')],
            'results' => ['required', new OverlapRule(), new MisRangeRule(), new MinRangeCanBeRule(), new MaxRangeCanBeRule()],
            'results.*.min_mark' => ['required_with:max_mark', 'numeric'],
            'results.*.max_mark' => ['required:_with:min_mark', 'numeric', 'gte:results.*.min_mark'],
            'results.*.mark_name' => ['required',],
        ];
    }
}
