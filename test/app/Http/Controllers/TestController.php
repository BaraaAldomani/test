<?php

namespace App\Http\Controllers;

use App\DTOs\TestData;
use App\Http\Actions\Test\CreateTestAction;
use App\Http\Requests\CreateTestRequest;
use App\Http\Resources\TestResource;
use App\services\AnswerService;
use App\services\QuestionService;
use App\services\ResultService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\returnArgument;

class TestController extends Controller
{
    public function store(CreateTestRequest $request)
    {
        try {
            DB::beginTransaction();
            $testData = TestData::fromRequest($request);
            $testData->point_of_answers = $testData->pointsToJson();

            $test = (new CreateTestAction())->execute($testData->toArray());

            (new QuestionService())->createMultipleQuestion($request['questions'], $test);

            (new ResultService())->createMultipleResults($request['results'], $test);
            $test->load('questions.answers', 'results');
            DB::commit();
            return $this->responseSuccess(new TestResource($test));
        }catch (\Exception $exception){
            DB::rollBack();
            return $this->responseError();
        }
    }
}
