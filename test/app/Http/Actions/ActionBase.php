<?php

namespace App\Http\Actions;

interface ActionBase
{
    public function execute(array $data);
    public function logger();

}
