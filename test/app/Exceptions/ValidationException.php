<?php

namespace App\Exceptions;

use App\Traits\ApiResponderTrait;
use Exception;
use Throwable;

class ValidationException extends Exception
{
    use ApiResponderTrait;
    private array $errors;
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null, array $errors = [])
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    function render(){
       return $this->responseBadRequest($this->errors);
    }
}
