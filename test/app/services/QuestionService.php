<?php

namespace App\services;

use App\Models\Question;
use App\Models\Test;

class QuestionService
{

    public function createMultipleQuestion($questionsData,Test $test){
       $questions = $test->questions()->createMany($questionsData);
       foreach ($questions as  $key=>$question){
           (new AnswerService())->createMultipleAnswers($questionsData[$key]['answers'],$question, $test);
       }
    }

}
