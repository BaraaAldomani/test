<?php

namespace App\services;

use App\Enums\TestTypes;
use App\Models\Question;
use App\Models\Test;

class AnswerService
{
    public function createMultipleAnswers($answers, Question $question, Test $test){
        $answersData = array();
        foreach ($answers as  $key=>$answer){
            $newAnswer = [];
            $newAnswer['answer_text'] = $this->checkOnTestTypeToGetAnswer($answer,$key,$test);
            $points = array_values($test->point_of_answers);
            $newAnswer['point_of_answer'] =(int)$points[$key];
            $answersData[$key]= $newAnswer;
        }

        $question->answers()->createMany($answersData);
    }

    public function checkOnTestTypeToGetAnswer($answer,$key,$test){
        if ($test->type == TestTypes::TRUE_FALSE->value){
            if ($key==0){
                return 'Yes';
            }else{
                return 'No';
            }
        }
        return $answer;
    }

}
