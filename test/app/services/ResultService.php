<?php

namespace App\services;

use App\Models\Test;

class ResultService
{
    public function createMultipleResults($results,Test $test){

        $test->results()->createMany($results);
    }

}
